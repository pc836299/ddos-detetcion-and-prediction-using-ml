
# -*- coding: utf-8 -*-
"""
Created on Fri Aug  9 09:14:33 2024

@author: soura
"""
import pandas as pd
import numpy as np
from sklearn.preprocessing import LabelEncoder, StandardScaler
from sklearn.decomposition import PCA
import joblib
import time
import warnings
import matplotlib.pyplot as plt
from collections import Counter
from playsound import playsound  



# File path to the new dataset
new_dataset_path = r"C:\Users\soura\OneDrive\Desktop\Disstertation\4th darft\Dataset\Cross-Validating Dataset.csv"

# Load the new dataset
new_df = pd.read_csv(new_dataset_path)


new_df

# Perform the same preprocessing steps as the original dataset




# Remove leading and trailing spaces from column names
new_df.columns = new_df.columns.str.strip()

# Drop the specified columns
# Drop the specified columns, including the ones you mentioned
columns_to_drop = ['Unnamed: 0', 'SimillarHTTP', 'Inbound', 'Flow ID', 'Timestamp']
new_df = new_df.drop(columns=columns_to_drop, axis=1, errors='ignore')

new_df.columns
new_df
#df = df.drop(['Flow ID', 'Timestamp'], axis=1)


# Initialize the label encoder
label_encoder = LabelEncoder()

# Encode the 'Source IP' and 'Destination IP' columns
if 'Source IP' in new_df.columns:
    new_df['Source IP'] = label_encoder.fit_transform(new_df['Source IP'])
if 'Destination IP' in new_df.columns:
    new_df['Destination IP'] = label_encoder.fit_transform(new_df['Destination IP'])

# Encode the 'Label' column ('DDoS' is 1 and 'Benign' is 0)
if 'Label' in new_df.columns:
    new_df['Label'] = label_encoder.fit_transform(new_df['Label'])

# Split the dataset into X (features) and y (target variable)
X_new = new_df.drop('Label', axis=1, errors='ignore')  # Drop the 'Label' column to get the features
y_new = new_df['Label'] if 'Label' in new_df.columns else None  # Select the 'Label' column as the target variable if it exists

# Check for NaN values and replace them with zeros
X_new = X_new.replace([np.inf, -np.inf], np.nan).fillna(0)

# Scale the features
scaler = StandardScaler()
X_new_scaled = scaler.fit_transform(X_new)

# Apply PCA if your original dataset had PCA applied
pca = PCA(n_components=10)  # Use the same number of components as your original PCA
X_new_pca = pca.fit_transform(X_new_scaled)

# Create a DataFrame from the PCA-transformed data
X_new_pca_df = pd.DataFrame(X_new_pca, columns=[f'PC{i+1}' for i in range(10)])
# Load the trained models
models = {
    'Random Forest': joblib.load('random_forest_model.pkl'),
    'XGBoost': joblib.load('xgboost_model.pkl'),
    'Logistic Regression': joblib.load('logistic_regression_model.pkl'),
    'Support Vector Machine': joblib.load('support_vector_machine_model.pkl'),
    'K-Nearest Neighbors': joblib.load('k-nearest_neighbors_model.pkl'),
    'Decision Tree': joblib.load('decision_tree_model.pkl'),
    'Naive Bayes': joblib.load('naive_bayes_model.pkl'),
    'Gradient Boosting': joblib.load('gradient_boosting_model.pkl')
}

warnings.filterwarnings("ignore", category=UserWarning, module="sklearn")

# Real-time simulation function with port detection, cleaner output, occupancy count, pie chart, and mean accuracy calculation
def simulate_real_time_prediction_with_ringtones(data, labels, models, original_data, delay=1):
    original_ports = original_data[['Source Port', 'Destination Port']]
    vulnerable_ports = {}
    model_accuracies = {name: [] for name in models.keys()}  # Dictionary to store accuracies for each model

    for index, row in data.iterrows():
        print(f"\nProcessing data point {index + 1}/{len(data)}")
        predictions = {}
        ddos_detected = False
        row_ports = tuple(original_ports.iloc[index])

        for name, model in models.items():
            prediction = model.predict(row.values.reshape(1, -1))
            predictions[name] = 'DDoS' if prediction[0] == 1 else 'Benign'
            model_accuracies[name].append(prediction[0] == labels.iloc[index])  # Store whether the prediction was correct
            if prediction[0] == 1:
                ddos_detected = True
                if row_ports in vulnerable_ports:
                    vulnerable_ports[row_ports] += 1
                else:
                    vulnerable_ports[row_ports] = 1
            print(f"- {name}: {predictions[name]} (Source Port: {row_ports[0]}, Destination Port: {row_ports[1]})")

        if ddos_detected:
            print(f"** Alert: Potential DDoS attack detected at data point {index + 1} **")

        time.sleep(delay)  # Simulate delay between data points

    # Calculate and print mean accuracy for each model
    mean_accuracies = {name: np.mean(accuracies) for name, accuracies in model_accuracies.items()}
    print("\nMean Accuracy for each Model:")
    for name, accuracy in mean_accuracies.items():
        print(f"- {name}: {accuracy:.4f}")

    # Sort vulnerable ports by frequency of attacks
    sorted_vulnerable_ports = Counter(vulnerable_ports).most_common(3)

    print("\nSummary of Top 3 Most Frequent Vulnerable Ports:")
    print("-----------------------------------------------------------")
    for i, ((src_port, dest_port), count) in enumerate(sorted_vulnerable_ports):
        print(f"Rank {i + 1} -> Source Port: {src_port}, Destination Port: {dest_port} - Attack Attempts: {count}")
        # Play different ringtones based on rank
        if i == 0:
            playsound(r"C:\Users\soura\OneDrive\Desktop\Disstertation\4th darft\4th darft\1.mp3")  
        elif i == 1:
            playsound(r"C:\Users\soura\OneDrive\Desktop\Disstertation\4th darft\4th darft\2.mp3")  
        elif i == 2:
            playsound(r"C:\Users\soura\OneDrive\Desktop\Disstertation\4th darft\4th darft\3.mp3")  

    print("-----------------------------------------------------------")

    # Plotting the pie chart for top 3 attacks
    labels = [f'Source: {src_port}, Dest: {dest_port}' for (src_port, dest_port), _ in sorted_vulnerable_ports]
    sizes = [count for _, count in sorted_vulnerable_ports]

    plt.figure(figsize=(10, 7))
    plt.pie(sizes, labels=labels, autopct='%1.1f%%', startangle=140)
    plt.axis('equal')  # Equal aspect ratio ensures that pie is drawn as a circle.
    plt.title('Top 3 Most Frequent Attack Attempts on Vulnerable Ports')
    plt.show()

# Simulate with the new dataset and original port data
simulate_real_time_prediction_with_ringtones(
    pd.DataFrame(X_new_pca_df), 
    y_new if y_new is not None else pd.Series([None] * len(X_new_pca_df)), 
    models, 
    new_df[['Source Port', 'Destination Port']]
)
