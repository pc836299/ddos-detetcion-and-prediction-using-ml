
#----------------------------import lib----------------------------------------

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.preprocessing import LabelEncoder, StandardScaler
from imblearn.under_sampling import RandomUnderSampler
from sklearn.feature_selection import VarianceThreshold, SelectKBest, f_classif
from sklearn.decomposition import PCA
from sklearn.model_selection import train_test_split, cross_val_score, StratifiedKFold
from sklearn.ensemble import RandomForestClassifier, GradientBoostingClassifier
from xgboost import XGBClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.svm import SVC
from sklearn.neighbors import KNeighborsClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.metrics import confusion_matrix, accuracy_score, classification_report, roc_curve, auc
#import tensorflow as tf
from tensorflow import keras
from tensorflow.keras.layers import Dropout
import gym
from gym import spaces
import joblib
import time
from sklearn.metrics import confusion_matrix, precision_recall_curve, roc_curve, auc
from collections import Counter
from sklearn.metrics import precision_score, recall_score, f1_score
#from sklearn.base import clone
from sklearn.model_selection import learning_curve

#--------------------------File Import-----------------------------------------
# File path
file_path = r"C:\Users\soura\OneDrive\Desktop\Disstertation\4th darft\Dataset\Friday-WorkingHours-Afternoon-DDos.pcap_ISCX.csv"
# Read the CSV file
df = pd.read_csv(file_path)

#------------------------------------------------------------------------------

df

df.info()

df.columns


# Check for missing values
missing_values = df.isnull().sum()
print("Missing Values:\n", missing_values)


# Iterate over columns with missing values
columns_with_missing_values = []
for column, count in missing_values.items():
    if count > 0:
        columns_with_missing_values.append(column)

# Print columns with missing values
print("Columns with Missing Values:")
for column in columns_with_missing_values:
    print(f"{column}: {missing_values[column]} missing values")
    
    
# as we have less number of Missing Values, We will be dropping the rows with missing values
df = df.dropna()

#----------------------------------EDA-----------------------------------------

# Summary statistics for numeric columns
print("\nSummary Statistics:")
print(df.describe())

# Box plots for numeric columns
numeric_cols = df.select_dtypes(include=np.number).columns
for col in numeric_cols:
    plt.figure(figsize=(6, 4))
    sns.boxplot(data=df, x=col)
    plt.title(f'Box Plot of {col}')
    plt.show()



#----------------------------Data Preprocessing--------------------------------


# Remove leading and trailing spaces from column names
df.columns = df.columns.str.strip()

# Drop the 'Flow ID' and 'Timestamp' columns
df = df.drop(['Flow ID', 'Timestamp'], axis=1)

df.info()



# Initialize the label encoder
label_encoder = LabelEncoder()

# Encode the 'Source IP' column
df['Source IP'] = label_encoder.fit_transform(df['Source IP'])

# Encode the 'Destination IP' column
df['Destination IP'] = label_encoder.fit_transform(df['Destination IP'])

# Encode the 'Label' column (assuming 'DDoS' is 1 and 'Benign' is 0)
df['Label'] = label_encoder.fit_transform(df['Label'])


df


df.info()

# Correlation heatmap for numeric columns
correlation_matrix = df.corr()
plt.figure(figsize=(20, 20))
sns.heatmap(correlation_matrix, annot=True, cmap='coolwarm', fmt=".2f")
plt.title('Correlation Heatmap')
plt.show()


# Split the dataset into X (features) and y (target variable)
X = df.drop('Label', axis=1)  # Drop the 'Label' column to get the features
y = df['Label']  # Select the 'Label' column as the target variable

# Initialize the RandomUnderSampler
rus = RandomUnderSampler(random_state=42)

# Apply the RandomUnderSampler to balance the classes
X_downsampled, y_downsampled = rus.fit_resample(X, y)

# Check the class distribution
print(y_downsampled.value_counts())

y = y_downsampled

# Separate the port columns that should not be scaled
port_columns = ['Destination Port', 'Source Port']
X_ports = X_downsampled[port_columns]

# Drop the port columns from the features to be scaled
X_features = X_downsampled.drop(columns=port_columns)

# Check for NaN values and replace them with zeros
X_features = X_features.replace([np.inf, -np.inf], np.nan).fillna(0)

# Apply StandardScaler to the remaining features
scaler = StandardScaler()
X_scaled = scaler.fit_transform(X_features)

# Create a new DataFrame with the scaled values and columns from X_features
X_scaled_df = pd.DataFrame(X_scaled, columns=X_features.columns)

# Recombine the non-scaled port columns with the scaled features
X_final = pd.concat([X_scaled_df, X_ports.reset_index(drop=True)], axis=1)


#---------------------Feature Selection----------------------------------------

# Initialize the VarianceThreshold selector with a threshold of 0
selector = VarianceThreshold(threshold=0)

# Fit the selector to your scaled data
selector.fit(X_scaled_df)

# Get the indices of non-constant features
non_constant_indices = selector.get_support(indices=True)

# Select only the non-constant features
X_non_constant = X_scaled_df.iloc[:, non_constant_indices]



X_non_constant


# Select the top k features based on ANOVA F-statistic
k_best = SelectKBest(score_func=f_classif, k=40)  # Adjust 'k' as needed
X_new = k_best.fit_transform(X_non_constant, y)


# Create a DataFrame with the selected features
selected_feature_names = X_non_constant.columns[k_best.get_support()]
X_selected = pd.DataFrame(X_new, columns=selected_feature_names)


X_selected


#--------------------------------PCA-------------------------------------------


# Save the original data before PCA for later port analysis
X_train_orig, X_test_orig, y_train_orig, y_test_orig = train_test_split(
    X_selected, y, test_size=0.2, random_state=42
)

# Initialize PCA with a large number of components
pca = PCA()

# Fit PCA to your data
pca.fit(X_selected)

# Plot the explained variance ratio
explained_variance_ratio = pca.explained_variance_ratio_
plt.plot(range(1, len(explained_variance_ratio) + 1), explained_variance_ratio.cumsum(), marker='o', linestyle='--')
plt.xlabel('Number of Components')
plt.ylabel('Cumulative Explained Variance')
plt.title('Explained Variance vs. Number of Components')
plt.grid()
plt.show()

# Initialize PCA with the number of components
n_components = 10  # elbow point of the Graph is 10
pca = PCA(n_components=n_components)

# Fit and transform the data
X_pca = pca.fit_transform(X_selected)

# Create a DataFrame from the PCA-transformed data
X_pca_df = pd.DataFrame(X_pca, columns=[f'PC{i+1}' for i in range(n_components)])

#------------------------Classification Model Training with Learning Curves------------------------

# Split the PCA-transformed data into training and testing sets (80% train, 20% test)
X_train, X_test, y_train, y_test = train_test_split(X_pca_df, y, test_size=0.2, random_state=42)

# Define a list of classifiers
classifiers = {
    'Random Forest': RandomForestClassifier(),
    'XGBoost': XGBClassifier(),
    'Logistic Regression': LogisticRegression(),
    'Support Vector Machine': SVC(probability=True),
    'K-Nearest Neighbors': KNeighborsClassifier(),
    'Decision Tree': DecisionTreeClassifier(),
    'Naive Bayes': GaussianNB(),
    'Gradient Boosting': GradientBoostingClassifier()
}

# Function to plot learning curves
def plot_learning_curve(estimator, title, X, y, axes=None, ylim=None, cv=None, n_jobs=None, train_sizes=np.linspace(.1, 1.0, 5)):
    if axes is None:
        _, axes = plt.subplots(1, 1, figsize=(10, 6))

    axes.set_title(title)
    if ylim is not None:
        axes.set_ylim(*ylim)
    axes.set_xlabel("Training examples")
    axes.set_ylabel("Score")

    train_sizes, train_scores, test_scores = learning_curve(
        estimator, X, y, cv=cv, n_jobs=n_jobs, train_sizes=train_sizes
    )
    train_scores_mean = np.mean(train_scores, axis=1)
    train_scores_std = np.std(train_scores, axis=1)
    test_scores_mean = np.mean(test_scores, axis=1)
    test_scores_std = np.std(test_scores, axis=1)

    axes.grid()

    axes.fill_between(train_sizes, train_scores_mean - train_scores_std,
                      train_scores_mean + train_scores_std, alpha=0.1,
                      color="r")
    axes.fill_between(train_sizes, test_scores_mean - test_scores_std,
                      test_scores_mean + test_scores_std, alpha=0.1,
                      color="g")
    axes.plot(train_sizes, train_scores_mean, 'o-', color="r",
              label="Training score")
    axes.plot(train_sizes, test_scores_mean, 'o-', color="g",
              label="Cross-validation score")

    axes.legend(loc="best")

    return plt

# Train, evaluate, and save each classifier
for name, classifier in classifiers.items():
    classifier.fit(X_train, y_train)
    y_pred = classifier.predict(X_test)
    y_prob = classifier.predict_proba(X_test)[:, 1] if hasattr(classifier, "predict_proba") else classifier.decision_function(X_test)

    accuracy = accuracy_score(y_test, y_pred)
    precision = precision_score(y_test, y_pred)
    recall = recall_score(y_test, y_pred)
    f1 = f1_score(y_test, y_pred)

    fpr, tpr, _ = roc_curve(y_test, y_prob)
    roc_auc = auc(fpr, tpr)

    print(f"Classifier: {name}")
    print(f"Accuracy: {accuracy:.2f}")
    print(f"Precision: {precision:.2f}")
    print(f"Recall: {recall:.2f}")
    print(f"F1 Score: {f1:.2f}")
    print(f"ROC AUC: {roc_auc:.2f}")
    print(f"Classification Report:\n{classification_report(y_test, y_pred)}\n")

    # Plot ROC curve
    plt.figure()
    plt.plot(fpr, tpr, label=f'ROC curve (area = {roc_auc:.2f})')
    plt.plot([0, 1], [0, 1], 'k--')
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.05])
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.title(f'ROC Curve for {name}')
    plt.legend(loc="lower right")
    plt.show()

    # Plot Learning Curve
    plot_learning_curve(classifier, f'Learning Curves ({name})', X_train, y_train, cv=5)
    plt.show()

    # Save the trained model
    joblib.dump(classifier, f'{name.lower().replace(" ", "_")}_model.pkl')


    
#-----------------Cross Validation on Different Classifiers--------------------


# Define a list of classifiers
classifiers = {
    'Random Forest': RandomForestClassifier(),
    'XGBoost': XGBClassifier(),
    'Logistic Regression': LogisticRegression(),
    'Support Vector Machine': SVC(),
    'K-Nearest Neighbors': KNeighborsClassifier(),
    'Decision Tree': DecisionTreeClassifier(),
    'Naive Bayes': GaussianNB(),
    'Gradient Boosting': GradientBoostingClassifier()
}

# Set up cross-validation
cv = StratifiedKFold(n_splits=5, shuffle=True, random_state=42)

# Perform cross-validation for each classifier
for name, classifier in classifiers.items():
    scores = cross_val_score(classifier, X_pca_df, y, cv=cv, scoring='accuracy')
    
    print(f"Classifier: {name}")
    print(f"Mean Accuracy: {scores.mean():.2f}")
    print(f"Accuracy Std Dev: {scores.std():.2f}")
    print(f"Accuracy Scores: {scores}\n")
    
    
#------------------------------Deep Learning-----------------------------------    
    
# Split the data into training and testing sets (80% train, 20% test)
X_train, X_test, y_train, y_test = train_test_split(X_pca_df, y, test_size=0.2, random_state=42)

# Build the neural network model
model = keras.Sequential([
    keras.layers.Input(shape=(X_train.shape[1],)),
    keras.layers.Dense(64, activation='relu'),
    Dropout(0.2),  # Add dropout with a 20% dropout rate
    keras.layers.Dense(32, activation='relu'),
    Dropout(0.2),  # Add dropout with a 20% dropout rate
    keras.layers.Dense(1, activation='sigmoid')
])



# Compile the model
model.compile(optimizer='adam',               # Optimizer
              loss='binary_crossentropy',    # Loss function for binary classification
              metrics=['accuracy'])          # Evaluation metric

early_stopping = keras.callbacks.EarlyStopping(
    monitor='val_loss',  # Monitor validation loss
    patience=5,           # Number of epochs with no improvement after which training will be stopped
    restore_best_weights=True  # Restore the model weights from the epoch with the best validation loss
)


# Train the model with early stopping
history = model.fit(X_train, y_train,
                    epochs=100,            
                    batch_size=32,
                    validation_data=(X_test, y_test),
                    verbose=1,
                    callbacks=[early_stopping])  

# Evaluate the model on test data
y_pred = (model.predict(X_test) > 0.5).astype(int)
accuracy = accuracy_score(y_test, y_pred)
report = classification_report(y_test, y_pred)

print(f"Accuracy: {accuracy:.2f}")
print(f"Classification Report:\n{report}")

# Plot training history (accuracy and loss over epochs)
import matplotlib.pyplot as plt

plt.figure(figsize=(12, 4))
plt.subplot(1, 2, 1)
plt.plot(history.history['accuracy'], label='Training Accuracy')
plt.plot(history.history['val_accuracy'], label='Validation Accuracy')
plt.title('Training and Validation Accuracy')
plt.xlabel('Epochs')
plt.ylabel('Accuracy')
plt.legend()

plt.subplot(1, 2, 2)
plt.plot(history.history['loss'], label='Training Loss')
plt.plot(history.history['val_loss'], label='Validation Loss')
plt.title('Training and Validation Loss')
plt.xlabel('Epochs')
plt.ylabel('Loss')
plt.legend()

plt.show()

#------------------------------Reinforcement Learning--------------------------


# Define a custom environment
class CustomEnv(gym.Env):
    def __init__(self, data, labels):
        super(CustomEnv, self).__init__()
        self.data = data
        self.labels = labels
        self.current_step = 0
        self.action_space = spaces.Discrete(2)  # two actions: benign (0) or DDoS (1)
        self.observation_space = spaces.Box(low=0, high=1, shape=(data.shape[1],), dtype=np.float32)
    
    def reset(self):
        self.current_step = 0
        return self.data[self.current_step]
    
    def step(self, action):
        reward = 1 if action == self.labels[self.current_step] else -1
        self.current_step += 1
        done = self.current_step >= len(self.data)
        next_state = self.data[self.current_step] if not done else np.zeros(self.data.shape[1])
        return next_state, reward, done, {}

# Prepare the data
data = X_pca_df.to_numpy()
labels = y.to_numpy()

# Initialize the environment
env = CustomEnv(data, labels)

# Define the Q-learning algorithm with state indexing
class QLearningAgent:
    def __init__(self, state_size, action_size):
        self.state_size = state_size
        self.action_size = action_size
        self.q_table = np.zeros((state_size, action_size))
        self.learning_rate = 0.1
        self.gamma = 0.95
        self.epsilon = 1.0
        self.epsilon_decay = 0.995
        self.epsilon_min = 0.01
        self.state_index = {}
        self.state_counter = 0

    def get_state_index(self, state):
        state_tuple = tuple(state)
        if state_tuple not in self.state_index:
            self.state_index[state_tuple] = self.state_counter
            self.state_counter += 1
        return self.state_index[state_tuple]
    
    def choose_action(self, state):
        state_idx = self.get_state_index(state)
        if np.random.rand() <= self.epsilon:
            return np.random.choice(self.action_size)
        return np.argmax(self.q_table[state_idx])
    
    def learn(self, state, action, reward, next_state, done):
        state_idx = self.get_state_index(state)
        next_state_idx = self.get_state_index(next_state)
        q_update = reward + self.gamma * np.max(self.q_table[next_state_idx]) * (1 - done)
        self.q_table[state_idx, action] = self.q_table[state_idx, action] + self.learning_rate * (q_update - self.q_table[state_idx, action])
        if done:
            self.epsilon = max(self.epsilon_min, self.epsilon * self.epsilon_decay)

# Initialize the Q-learning agent
state_size = len(data)
action_size = env.action_space.n
agent = QLearningAgent(state_size, action_size)

# Train the agent
num_episodes = 1000

for e in range(num_episodes):
    state = env.reset()
    total_reward = 0
    done = False
    
    while not done:
        action = agent.choose_action(state)
        next_state, reward, done, _ = env.step(action)
        agent.learn(state, action, reward, next_state, done)
        state = next_state
        total_reward += reward
    
    print(f"Episode {e+1}/{num_episodes}, Total Reward: {total_reward}")

print("Training finished.\n")

# Evaluate the agent
state = env.reset()
done = False
total_reward = 0
y_true = []
y_pred = []

while not done:
    action = agent.choose_action(state)
    next_state, reward, done, _ = env.step(action)
    state = next_state
    total_reward += reward
    y_true.append(labels[env.current_step - 1])
    y_pred.append(action)

print(f"Total Reward during evaluation: {total_reward}")

# Calculate accuracy
accuracy = total_reward / len(data)
print(f"Accuracy: {accuracy:.2f}")

# Generate confusion matrix
conf_matrix = confusion_matrix(y_true, y_pred)
print(f"Confusion Matrix:\n{conf_matrix}")

# Plot confusion matrix
plt.figure(figsize=(8, 6))
sns.heatmap(conf_matrix, annot=True, fmt="d", cmap='Blues')
plt.xlabel('Predicted')
plt.ylabel('True')
plt.title('Confusion Matrix')
plt.show()









# Plot Action Distribution During Evaluation
# Assuming `y_pred` contains the predicted actions during evaluation
plt.figure(figsize=(8, 6))
actions = ['Benign', 'DDoS']
counts = [y_pred.count(0), y_pred.count(1)]  # Replace with your actual counts
sns.barplot(x=actions, y=counts, palette='viridis')
plt.xlabel('Action')
plt.ylabel('Count')
plt.title('Action Distribution During Evaluation')
plt.show()

# Optional: Plot Precision-Recall Curve
precision, recall, _ = precision_recall_curve(y_true, y_pred)
plt.figure(figsize=(8, 6))
plt.plot(recall, precision, marker='.')
plt.xlabel('Recall')
plt.ylabel('Precision')
plt.title('Precision-Recall Curve')
plt.grid(True)
plt.show()

# Optional: Plot ROC Curve
fpr, tpr, _ = roc_curve(y_true, y_pred)
roc_auc = auc(fpr, tpr)
plt.figure(figsize=(8, 6))
plt.plot(fpr, tpr, label=f'AUC = {roc_auc:.2f}', color='red')
plt.plot([0, 1], [0, 1], 'k--')  # Diagonal line
plt.xlabel('False Positive Rate')
plt.ylabel('True Positive Rate')
plt.title('ROC Curve')
plt.legend(loc='lower right')
plt.grid(True)
plt.show()
#----------------------------Model Comparison ---------------------------------

# Train other models
classifiers = {
    'Random Forest': RandomForestClassifier(),
    'XGBoost': XGBClassifier(),
    'Logistic Regression': LogisticRegression(),
    'Support Vector Machine': SVC(probability=True),
    'K-Nearest Neighbors': KNeighborsClassifier(),
    'Decision Tree': DecisionTreeClassifier(),
    'Naive Bayes': GaussianNB(),
    'Gradient Boosting': GradientBoostingClassifier()
}

# Split the data into training and testing sets
X_train, X_test, y_train, y_test = train_test_split(X_pca_df, y, test_size=0.2, random_state=42)

for name, classifier in classifiers.items():
    classifier.fit(X_train, y_train)

# Compare with other models
results = {
    'Model': [],
    'Accuracy': [],
    'Precision': [],
    'Recall': [],
    'F1 Score': []
}

for name, classifier in classifiers.items():
    y_pred_other = classifier.predict(X_test)
    accuracy_other = accuracy_score(y_test, y_pred_other)
    precision_other = precision_score(y_test, y_pred_other)
    recall_other = recall_score(y_test, y_pred_other)
    f1_other = f1_score(y_test, y_pred_other)
    
    results['Model'].append(name)
    results['Accuracy'].append(accuracy_other)
    results['Precision'].append(precision_other)
    results['Recall'].append(recall_other)
    results['F1 Score'].append(f1_other)
    
    print(f"Classifier: {name}")
    print(f"Accuracy: {accuracy_other:.2f}")
    print(f"Precision: {precision_other:.2f}")
    print(f"Recall: {recall_other:.2f}")
    print(f"F1 Score: {f1_other:.2f}")
    print()

# Add RL model to comparison
results['Model'].append('Q-Learning Agent')
results['Accuracy'].append(accuracy)
results['Precision'].append(precision_score(y_true, y_pred))
results['Recall'].append(recall_score(y_true, y_pred))
results['F1 Score'].append(f1_score(y_true, y_pred))

# Convert results to a DataFrame for better readability
results_df = pd.DataFrame(results)
print(results_df)


