
This project contains two Python scripts: main.py and Port_Test.py.

Datasets
- main.py uses the dataset: Friday-WorkingHours-Afternoon-DDos.pcap_ISCX.csv.
- Port_Test.py uses the dataset: Cross-Validating Dataset.csv.

Additional Files for Port_Test.py
- Port_Test.py also utilizes three music files: 1.mp3, 2.mp3, and 3.mp3.

Instructions:
While running the code, ensure that the correct file paths for the datasets and music files are updated in the respective scripts. Apart from this, the code should function as expected.